package com.bitbucket.alexander_danilenko.java_calculator;

/**
 * Fraction helper methods.
 */
public class FractionUtils {

    /**
     * Returns greatest common divisor.
     *
     * @return Greatest common divisor.
     */
    public static int getGreatestCommonDivisor(int a, int b) {
        // Наибольший общий делитель.
        return b == 0 ? a : getGreatestCommonDivisor(b, (a % b));
    }

    /**
     * Returns least common multiple for pair of values.
     *
     * @param a First value.
     * @param b Second value.
     * @return Least common multiple.
     */
    public static int getLeastCommonMultiple(int a, int b) {
        // Наименьшее общее кратное.
        return a / getGreatestCommonDivisor(a, b) * b;
    }

    /**
     * Adds one fraction to another.
     *
     * @param a First fraction.
     * @param b Second fraction.
     * @return Result fraction.
     */
    public static Fraction addition(Fraction a, Fraction b) {
        // ((Числитель 1 * Знаменатель 2) + (Числитель 2 * Знаменатель 1)) / (Знаменатель 1 * Знаменатель 2)
        int calculatedNumerator = ((a.getNumerator() * b.getDenominator()) + (b.getNumerator() * a.getDenominator()));
        int calculatedDeNominator = (a.getDenominator() * b.getDenominator());

        return new Fraction(calculatedNumerator, calculatedDeNominator);
    }

    /**
     * Substracts two fractions.
     *
     * @param a First fraction.
     * @param b Second fraction.
     * @return Result fraction.
     */
    public static Fraction subtraction(Fraction a, Fraction b) {
        // Получим наименьшее общее кратное.
        int leastCommonMultiple = FractionUtils.getLeastCommonMultiple(a.getDenominator(), b.getDenominator());

        // Высчитаем числители при общих знаменателях.
        a.setNumerator(a.getNumerator() * (leastCommonMultiple / a.getDenominator()));
        b.setNumerator(b.getNumerator() * (leastCommonMultiple / b.getDenominator()));

        // Выставим один знаменатель.
        a.setDenominator(leastCommonMultiple);
        b.setDenominator(leastCommonMultiple);

        // Результат вычитания.
        return new Fraction((a.getNumerator() - b.getNumerator()), leastCommonMultiple);
    }

    /**
     * Multiplicates two fractions.
     *
     * @param a First fraction.
     * @param b Second fraction.
     * @return Result fraction.
     */
    public static Fraction multiplication(Fraction a, Fraction b) {
        // (Числитель 1 * Числитель 2) / (Знаменатель 1 * Знаменатель 2).
        return new Fraction((a.getNumerator() * b.getNumerator()), (a.getDenominator() * b.getDenominator()));
    }

    /**
     * Divides two fractions.
     *
     * @param a First fraction.
     * @param b Second fraction.
     * @return Result fraction.
     */
    public static Fraction division(Fraction a, Fraction b) {
        // (Числитель 1 * знаминатель 2) / (Знаминатель 1 * Числитель 2)
        return new Fraction((a.getNumerator() * b.getDenominator()), (a.getDenominator() * b.getNumerator()));
    }

}
