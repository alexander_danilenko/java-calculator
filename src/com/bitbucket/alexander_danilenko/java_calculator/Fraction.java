package com.bitbucket.alexander_danilenko.java_calculator;

/**
 * Fraction class.
 */
public class Fraction {

    //region Properties.

    /**
     * Fraction numerator.
     */
    protected int numerator;

    /**
     * Fraction denominator.
     */
    protected int denominator;

    //endregion

    //region Getters & setters.

    /**
     * Returns numerator.
     *
     * @return int Numerator value.
     */
    public int getNumerator() {
        return numerator;
    }

    /**
     * Sets numerator.
     *
     * @param numerator New numerator value.
     */
    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    /**
     * Returns denominator.
     *
     * @return int Denominator value.
     */
    public int getDenominator() {
        return denominator;
    }

    /**
     * Sets numerator.
     *
     * @param denominator New denominator value.
     * @throws IllegalArgumentException In case if denominator equals 0.
     */
    public void setDenominator(int denominator) {
        if (denominator == 0) {
            throw new IllegalArgumentException("Argument 'denominator' is 0.");
        }

        this.denominator = denominator;
    }

    //endregion

    /**
     * Fraction constructor.
     *
     * @param numerator   Numerator value.
     * @param denominator Denominator value.
     */
    public Fraction(int numerator, int denominator) {
        this.setNumerator(numerator);
        this.setDenominator(denominator);

        // Shorten if its possible.
        this.shorten();
    }

    /**
     * Shorts current fraction.
     *
     * @return Current shorten fraction.
     */
    public Fraction shorten() {
        int foundGreatestCommonDivisor = FractionUtils.getGreatestCommonDivisor(this.getNumerator(), this.getDenominator());

        // Apply shorten to both parameters.
        this.setNumerator(this.getNumerator() / foundGreatestCommonDivisor);
        this.setDenominator(this.getDenominator() / foundGreatestCommonDivisor);

        return this;
    }

    /**
     * Adds fraction to current fraction object.
     *
     * @param f Fraction to add.
     * @return Result fraction.
     * @see FractionUtils#addition(Fraction, Fraction)
     */
    public Fraction add(Fraction f) {
        return FractionUtils.addition(this, f);
    }

    /**
     * Divides current fraction object with another fraction.
     *
     * @param f Fraction to divide.
     * @return Result fraction.
     * @see FractionUtils#division(Fraction, Fraction)
     */
    public Fraction divide(Fraction f) {
        return FractionUtils.division(this, f);
    }

    /**
     * Multiplicates current fraction object with another fraction.
     *
     * @param f Fraction to multiplicates.
     * @return Result fraction.
     * @see FractionUtils#multiplication(Fraction, Fraction)
     */
    public Fraction multiply(Fraction f) {
        return FractionUtils.multiplication(this, f);
    }

    /**
     * Multiplicates current fraction object with another fraction.
     *
     * @param f Fraction to subtract.
     * @return Result fraction.
     * @see FractionUtils#subtraction(Fraction, Fraction)
     */
    public Fraction subtract(Fraction f) {
        return FractionUtils.subtraction(this, f);
    }

    @Override
    public String toString() {
        return toString(this);
    }

    /**
     * Static toString method.
     *
     * @param f Input fraction.
     * @return Formatted string.
     */
    public static String toString(Fraction f) {
        // Если в числителе ноль.
        if (f.getNumerator() == 0) {
            return "0";
        }
        // Если в результате получается целое число.
        else if (f.getNumerator() % f.getDenominator() == 0) {
            return Integer.toString(f.getNumerator() / f.getDenominator());
        }
        // Если числитель или знаминатель меньше нуля.
        else if (f.getNumerator() < 0 || f.getDenominator() < 0) {
            return "-(" + Math.abs(f.getNumerator()) + "/" + Math.abs(f.getDenominator()) + ")";
        }

        return f.getNumerator() + "/" + f.getDenominator();
    }

}
