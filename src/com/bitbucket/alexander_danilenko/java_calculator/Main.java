package com.bitbucket.alexander_danilenko.java_calculator;

public class Main {

    /**
     * Main method.
     *
     * @param args
     *   Default args.
     */
    public static void main(String[] args) {
        Fraction a1 = new Fraction(-1,2);
        Fraction a2 = new Fraction(-1,2);

        System.out.printf("Первая дробь: %s\n", a1.toString());
        System.out.printf("Вторая дробь: %s\n", a2.toString());
        System.out.print("\n");

        // Add two fractions.
        Fraction additionResult = a1.add(a2);
        System.out.println("Результат сложения: " + additionResult.toString());

        Fraction substractionResult = a1.subtract(a2);
        System.out.println("Результат вычитания: " + substractionResult.toString());

        Fraction multiplicationResult = a1.multiply(a2);
        System.out.println("Результат умножения: " + multiplicationResult.toString());

        Fraction divisionResult = a1.divide(a2);
        System.out.println("Результат умножения: " + divisionResult.toString());
    }
}
