package com.bitbucket.alexander_danilenko.java_calculator.Forms;

import com.bitbucket.alexander_danilenko.java_calculator.Fraction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class FractionCalcForm {
    private JButton triggerCalculation;
    private JPanel jPanel;
    private JSpinner fraction1Numerator;
    private JSpinner fraction1Denominator;
    private JSpinner fraction2Numerator;
    private JSpinner fraction2Denominator;
    private JComboBox operation;
    private JLabel operationLabel;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FractionCalcForm");
        frame.setContentPane(new FractionCalcForm().jPanel);
        frame.setTitle("Fraction calculator");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(200, 200);
        frame.setGlassPane(new FractionCalcForm().jPanel);
        frame.setVisible(true);
    }

    public FractionCalcForm() {

        // "Calculate" button click handler.
        triggerCalculation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // If form validation fails.
                if (!validateCalcForm()) {
                    JOptionPane.showMessageDialog(null, "Please fill all input items", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                // Create two fractions from input values.
                Fraction f1 = new Fraction((int) fraction1Numerator.getValue(), (int) fraction1Denominator.getValue());
                Fraction f2 = new Fraction((int) fraction2Numerator.getValue(), (int) fraction2Denominator.getValue());

                Fraction result;
                switch (operation.getSelectedItem().toString()) {
                    case "+":
                        result = f1.add(f2);
                        break;

                    case "-":
                        result = f1.subtract(f2);
                        break;

                    case "*":
                        result = f1.multiply(f2);
                        break;

                    case "/":
                        result = f1.divide(f2);
                        break;

                    default:
                        JOptionPane.showMessageDialog(null, "Something went wrong! ALARM!", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                }

                JOptionPane.showMessageDialog(null, "Your result: " + result.toString(), "Result", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }

    /**
     * Checks if whole form is valid.
     *
     * @return True if form is valid. False otherwise.
     */
    public boolean validateCalcForm() {
        // Add to found values array list of all inputs.
        // All should be filled with values.
        ArrayList<Integer> valuesFound = new ArrayList<>();
        valuesFound.add((int) fraction1Numerator.getValue());
        valuesFound.add((int) fraction1Denominator.getValue());
        valuesFound.add((int) fraction2Numerator.getValue());
        valuesFound.add((int) fraction2Denominator.getValue());
        return !valuesFound.contains(0);
    }

}
